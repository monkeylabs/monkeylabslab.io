---
layout: default
title: Gardening
---
---
## Motivation

Next to where we live is a small piece of land that the land lord (MKB) has designated for tennants to grow whatever they want. My plan is to try and grow some native edible plants for anyone to pickup and eat.

Secondly, in late 2020 I went out and collected some acorns to try and show my girls how trees can grow from seed. To my surprise, they actually worked <i>very well</i> and we ended up with about 10 oak saplings, sparking a love for afforestation in me.

This log documents my efforts to:

* Try and make local tree propagation a cheap sustainable process, rather than a lucky fluke.
* Implement some permaculture techniques with said gardening lot.

---
## Posts

<ul>
	{% assign gardeningPosts = site.posts | where: 'category', 'gardening' %}
  {% for post in gardeningPosts %}
    <li>
			<a href="{{ post.url }}">({{ post.date | date_to_string }}) {{ post.title }}</a>
    </li>
  {% endfor %}
</ul>
