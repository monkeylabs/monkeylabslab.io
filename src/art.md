---
layout: art
title: Art
---
### Occaisionally I like to try my hand at pixel and voxel art
---

<!-- Photo Grid -->
	{% assign i=0 %}
	{% assign maxIPerRow=3 %}
	{% for image in site.static_files %}
		{% if image.path contains 'images/art' %}
			{% if i == 0%}
<div class="row"> 
			{% endif %}
	<div class="column">
		<img src="{{ site.baseurl }}{{ image.path }}">
	</div>
			{% if i == 0%}
			{% endif %}
			{% assign i = i | plus:1 %}
			{% if i==maxIPerRow %}
				{% assign i=0 %}
</div>
			{% endif %}
		{% endif %}
	{% endfor %}
