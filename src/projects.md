---
layout: default
title: Projects
---
---
## Motivation

Inspired by Jonathan Dowland's [25 things I would like to 3D print](https://jmtd.net/log/3d_print_list/) I decided to have a list of all the projects that I would like one day to work on

---
## The List

1. **An Operating System** -> This is now [MonkeyOS](/projects/monkeyOS.html)

2. **Data Collection for our indoor plants**
	* collect air temperature, hours of sunlight, intensity of sunlight, air humidity, soil humidity
	* would be great if it can also take a picture of the plant
	* hopefully solar powered

3. **Automated watering for our plants based on the above as well as type of plant**

4. **Machine Learning based thing that can take said data from above and predict "stuff" about the plant**

5. **Make my coffee machine perfect**
	* Fix the beeper beeping at the wrong time when its not done yet
	* make the beeper beep once instead of 6 times
	* lower beeper volume
	* dont actually stop the "keep warm" function until stop button pressed
	* maybe fallback stop the "keep warm" function if the weight detected matches weight for empty pot
	* 3d print a new enclosure and buy new pot that isnt for 12 mugs only, but also smaller quantities
	* calibrate new enclosure scale so 1 mug = my mug, and not some mythical tiny mug
		* current scale my mug = 4 coffee machine mugs

6. **RC submarine**

7. **RC sailing boat**

8. **Collapsable / packable sailing boat that i can pack down and transport in a bike-pulled cart**

9. **Shaving horse for helping make wooden bows**
