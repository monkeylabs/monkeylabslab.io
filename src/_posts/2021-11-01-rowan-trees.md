---
layout: post
category: gardening
---

Around our home, there are four trees that turn a brilliant shade of red in autumn, and I find them just beautiful.

So naturally, my thoughts started wandering into the territory of propagating them.

First step was identifying the trees. After a quick wikipedia dive, I have come to the conclusion that they are likely [Sorbus aucuparia, commonly called rowan or mountain-ash in the UK](https://en.wikipedia.org/wiki/Sorbus_aucuparia)

Second step is to go out and gather some of the fruit. I am not entirely sure what time is best to gather the fruit of said tree, but since wikipedia says the fruit ripens from August to October, then I think now is a safe bet.

All of these fruits were gathered from the ground underneath the tree. I thought the fruits still on the tree might not be ready yet, so I thought picking the freshly fallen ones would be best.

![The seeds collected from the four local trees](/assets/images/gardening/6_rowan_gathered_fruits.jpg)

Next up, each fruit was split in half and opened up

![One of the fruits split in half and opened up](/assets/images/gardening/7_rowan_fruit_split_open.jpg)

Digging through the fruit a pair of tweezers, one or two seeds are taken out of each fruit. There are more seeds in there, but they seemed just hollow shells. Only one or two seeds seemed like they contained an actual embryo.

Do that repeatedly for all the fruits, and you get some nice bunch of seeds. (1 pence coin in photo for scale)

![The seeds taken out of all the fruits](/assets/images/gardening/8_rowan_seeds_taken_out.jpg)

![Closeup of the seeds](/assets/images/gardening/9_rowan_seeds_closeup.jpg)

Some of the fruits look very fresh, and some are wrinkly. I could not find a consistent pattern between the wrinkliness and number of seeds contained in the fruit.

Then, I took all the seeds and put them in a deep cup. I used the method I use with the oaks: soak them in water, discard the ones that float.

Thankfully, only three of them floated, so there was not a huge loss of seeds.

![Float testing the seeds](/assets/images/gardening/10_rowan_seeds_float_test.jpg)

And finally, to make sure they don't rot, I tried washing off all the excess fruit meat that was sticking to the seeds. Then I will leave them out to dry for a day before I try planting them in soil.

![Drying out the fruit meat around the seeds](/assets/images/gardening/11_rowan_seeds_drying.jpg)

*Note: I know these images don't convey scale much, but these seeds are about 1-2mm in length each. It is mind-blowing that these can grow to a tree that can be up to 12m tall one day!*
