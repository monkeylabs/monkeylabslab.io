---
layout: post
category: gardening
---

I moved all nine planter pots outside and arranged them around the lot

![the marigold planters outside](/assets/images/gardening/ringblommor_20210417_outside.jpg)
