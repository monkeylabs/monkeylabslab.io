---
layout: post
category: gardening
---

Close to the place where my daughters go for tennis training, there are a lot of those trees that drop what our family calls "helicopter fruits". The fruits have a single leaf attached to them and they are offset. 

![The helicopter fruit](/assets/images/gardening/12_helicopter_fruit.jpg)

As they fall, the leaf rotates and turns into this rotating wing, that allows the fruit to fly far away from the tree. My daughter & I captured a video of the action that you can see [here](https://www.instagram.com/p/CTHe-wmsry6/).

![A closeup of the fruit](/assets/images/gardening/13_tilia_fruit_closeup.jpg)

As far as identification goes, after some searching online, I am about 80% certain these are the fruits of the [Tilia × europaea, or common lime](https://en.wikipedia.org/wiki/Tilia_%C3%97_europaea) tree.

We collected a bunch of the fruits, and then I got to removing the fruit from the "wing".

![The cleaned up fruits to scale](/assets/images/gardening/14_tilia_fruits.jpg)

Then using a pair of pliers, I started removing the outer hard shell of the fruit to get to the seed.

I have decided I will remove the hardshell from most of them, but I also wanted to leave 5 fruits intact, to see if they can germinate without being manually cracked.

If I were to guess, I'd gander in nature these fruits are probably cracked when the ground freezes over winter, like with cherry and plum seeds. But This is pure speculation

![Cleand up seeds and original fruits](/assets/images/gardening/15_tilia_seed_and_fruit.jpg)

In addition to those Tilia seeds, we also collected a bunch more Rowan. The reason is because I want to try and germinate the seeds just by squeezing the fruit by hand and planting it in soil.

If this works to germinate Rowan trees, then this would be a genuinely easier process than the one I had to do last week.

![All the fruits collected](/assets/images/gardening/16_all_fruits_collected.jpg)

Last, but not least, next to where we live, there is a street that is lined with a specific variety of tree that vearas fruit that _really_ reminded me of the fruit of the [Delonix regia, or the Royal Poinciana](https://en.wikipedia.org/wiki/Delonix_regia) tree.

I very, very highly doubted this would be the Delonix regia, because it is decidious, does not have the distinctive red flower and it is _way_ too cold here for these trees to be growing this well.

In any case, I was intrigued, so my daughters and I went out and collected some of the fallen fruit from that tree.

The fruit is usually in long strips and not circular or coiled like this one we picked, but this one was the _only_ one that seemed to have seed in it

When opened up, there were, indeed, seeds in there.

![Coiled up fruit opened](/assets/images/gardening/17_coiled_fruit_opened.jpg)

I took the seeds out and cleaned them. I plan to also, as you might have guessed with this entire blog, try and germinate them :)

![The mystery tree seeds](/assets/images/gardening/18_coiled_seeds.jpg)

Without almost any leaves on the tree left now, it can be a little tricky to identify the exact species.

However, that does not take away from the ability to plant it and try to germinate it :D

As always, stay tuned.
