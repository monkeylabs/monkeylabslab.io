---
layout: post
category: gardening
---

I have started experimenting a little with the marigolds. 

Originally, I had all nine planter pots outside, as you can see in the previous entry's image. 

However, due to my innate ability to ever leave something as it is, I started wondering if this was the best method to get some nice marigolds :). So, I took one of the nine planters and moved the seedlings directly into the ground

![the marigolds sown directly into the soil](/assets/images/gardening/ringblommor_20210417_direct_sow.jpg)

Now most people would be content with this kind of experimentation, however, patience has never been a virtue of mine.

Today as I was going about the house, I noticed that inside one of the planters where we are growing Kiwi from seed (long story, dont ask :D) one Marigold seedling had sprouted. And not just sprouted, but this seedling, growing under an ikea growth light for 16 hours a day, seems to be progressing much faster than the ones outside. 

This is not very surprising, as it has not yet been very sunny here in Sweden, so as of April 21st, the artificial light trumps sunlight, especially in the lot where it gets a lot of shade from the surrounding buildings.

![the marigold growing under artificial growth light. Please ignore the kiwi plant next to it :)](/assets/images/gardening/ringblommor_20210421_superbloom.jpg)

So, I have moved three planters of the marigolds indoors, under the growth lights. 

![the marigold planter pots moved under artificial ikea growth lights indoors, 16 hours a day on a timer](/assets/images/gardening/ringblommor_20210421_artificial_light.jpg)

I will continue to monitor the progress on the 3 different batches (Indoor under growth light, outdoor in a pot, outdoor direct in the soil) and try and see which batch will fare best, to try and optimise planting for next year. 

The plan is to one day pair these results with weather data as well as some sensors in order to try and match these conditions and try and train a NN that would predict the best times and methods for planting the different plants based on time of year and weather conditions and forecast... 

But that is a different story for a different day :) 
