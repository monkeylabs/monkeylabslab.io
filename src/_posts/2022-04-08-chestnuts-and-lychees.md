---
layout: post
category: gardening
---

### Chestnuts!

Last time, I had promised some very exciting news about some chestnuts. Unfortunately, I got very busy in the last couple of months, and could not write an update. But hey, better late than never!

Around Autumn each year, my wife enjoys oven baked Chestnuts. These tend to come from either Turkey or Spain or Italy.

Naturally, My first thought was to try and grow them.

As you might have read in the previous post, a mouse snuck into the balcony and ate all of the chestnuts that were planted outside.

So this year, I started their cold stratification indoors and when they started rooting, I put them under the indoor growth lights.

In a few days, they sprouting and showing some nice signs of being all ready for spring :)

![Closeup of sprouting Italian Chestnut](/assets/images/gardening/chestnuts_and_lychees/chestnuts_closeup_0.jpg)

![Closeup of sprouting Italian Chestnut](/assets/images/gardening/chestnuts_and_lychees/chestnuts_closeup_1.jpg)

As you can tell, some of them were clearly wanting more sunlight and were really reaching for the light

![Chestnut Stretch and Reaching](/assets/images/gardening/chestnuts_and_lychees/chestnuts_reaching.jpg)

However, I moved them from underneath the growth lights and into our kitchen window where they get a decent amount of morning sun.

In addition, we were blessed with quite the sunny (for Sweden) March, and since I work from home, everyday I would move them out into the balcony for a few hours where they would get a good sun bath.

![Sunbathing Chestnuts](/assets/images/gardening/chestnuts_and_lychees/chestnuts_sunbathing.jpg)

This is how they look as of today, on Week 14

![Four mighty fine looking italian chetnut saplings](/assets/images/gardening/chestnuts_and_lychees/chestnuts_w14.jpg)

---

### Lychees!

Now, this post can't be about Chestnuts and Lychees if we're not going to mention lychees are we?

So, from last time, the last update was that we had planted the lychees into some soil

About six of them sprouted and four of them have survived so far

![Closeup of sprouting Lychees](/assets/images/gardening/chestnuts_and_lychees/lychees_closeup_0.jpg)

![Closeup of sprouting Lychees](/assets/images/gardening/chestnuts_and_lychees/lychees_closeup_1.jpg)

Those however I have completely kept under the growth lights, as I don't think they would be able to take _any_ of the Swedish spring weather.

And as with the chestnuts, this is how the lychees look as of this writing.

![Four might fine looking Lychee plants](/assets/images/gardening/chestnuts_and_lychees/lychees_w14.jpg)

---

That is all for now. Spring is now coming in fast, and so there are a lot of things happening, so expect a lot of updates

From apples to oaks to maples to cherries to horse chestnuts, almost everything is waking up now.

Not to mention, there are all the flower and plum seeds that we collected last year.

As well as all the vegetable seeds for our little farming plot where our friend Mimi from [Gardener's little helper](https://www.facebook.com/gardeners.little.helper/) is helping us grow food in.

So, exciting times! Stay tuned!
