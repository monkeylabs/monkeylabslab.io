---
layout: post
category: gardening
---

Since I have run out of milk cartons to transport the acorns, I have decided to try and grow them in a water jar, like I do with avocado seeds

So, the plan became clear, dangle the seeds above a bit of water, while making sure the taproots are submerged

I am not entirely sure this will yeild proper trees ready to be transplanted into the ground, but I think it might yield some pretty interesting indoor plants. Plus, watching roots grow is always fun!

The next problem because how to suspend the acorns. For avocado seeds, I usually stab them with three tooth picks and hand over the water, however the acorns are a little too small for that

Hence, I opted to try and suspend them using coffee filters (since I use a metal mesh one, these filters are useless for me and have been sitting idle in my cupboards since we bought the filter coffee machine about 4 years ago)

I have about 20 acorns growing now in this way in varying jar sizes. I will keep an eye on them and see how it goes over the next few weeks

I don't actually know for a fact what makes something a proper hydroponic setup, but since these oaks are essentially now floating in water, rather than in proper soil, I have dubbed them "Hydroponic Oaks" :)

![The acorns hanging in their coffee filters with the roots in the water](/assets/images/gardening/4_hydroponic_oaks.jpg)

![The acorns dangling the taproot into the water](/assets/images/gardening/5_hydroponic_oaks.jpg)
