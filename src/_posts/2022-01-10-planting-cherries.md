---
layout: post
category: gardening
---

Continuing from the work that needed to be done I talked about in last post, this time it was the cherry seeds.

Across from our place, there grows a cherry tree that bears some of the most delicious (if small) cherry friut. This actually occurs annually _right around_ my birthday, so I've always felt a little extra something for that tree, so naturally, I tried to propagate it :)

So last year, around week 21 we went out and collected a lot of the ripe cherries. After enjoying them, we proceeded to clean the seeds and then used a vice grip to very carefully crack the outer hard shell. You can check out a video of this [on my instagram](https://www.instagram.com/p/CQ9cCAbL9OL/) or check out the image below.

![Cracking the cherry seeds with a vice](/assets/images/gardening/cherries/00_cracking_seeds.jpg)

After that, we stuffed them in a damp paper towel and put them in the fridge for the needed ~70 days of cold stratification. I checked on then around week 30 (~70 days later) and lo and behold they had indeed germinated. I took the best of them and planted them in small planters in the office. I actually don't know their state right now, since I WFH 100%, but I can only imagine :(

The rest, I just stuffed back into the paper towel, moved the box to the balcony and sort of forgot about them.

Lo and behold, 10 more weeks later, I was checking on them and they had sprouted furiously!

![Forgotten Cherry seeds](/assets/images/gardening/cherries/0_forgotten_cherry_seeds.jpg)

So I didn't really know what to do with them, so I stuck them, wet towel and all (foreshadowing, this will bite me in the arse later) into some soil and left them out in the balcony.

![Cherry Seeds planted](/assets/images/gardening/cherries/1_cherry_seeds_planted.jpg)

But that was last year, and this year I was looking at them and I felt bad about how much they seemed to be fighting to survive and how resilient they were, while I just crammed them in this tiny space with their roots all over each other

![Crammed Cherry seeds at W2](/assets/images/gardening/cherries/2_cherries_at_W2.jpg)

First, and here is where keeping them in the wet damp towel was a bad idea, I had to pull them apart. The problem of course is that the roots were all tangled up and were also quite embedded in the towel. The later was not much of a problem, as I did not care if the towel was ripped apart, but the former, the tangled roots, was the issue.

![Seeds pulled out from the soil](/assets/images/gardening/cherries/3_pulled_out.jpg)

I then separated them as much as I could, but the tangled roots means I lost about 50% of them. It was no surprise to me, and I knew this was going to happen once I took them out of the soil.

With that in mind, I tried to make sure that the ones breaking were the weaker and smaller sprouts anyway

I pulled them out, repotted each into its neat little pot and placed them out in the balcony.

![Planted Seeds](/assets/images/gardening/cherries/4_planted_ones.jpg)

![Closeup 0](/assets/images/gardening/cherries/5_closeup_0.jpg)

![Closeup 1](/assets/images/gardening/cherries/6_closeup_1.jpg)

UPDATE: So a few days after the move, I wanted to try and see if they would survive outside the balcony, since they *are* local trees. Unfortunately, I was stupid and did not realise that they need acclimatisation between comfortable indoor temperatures, and Swedish outdoor temperatures.

Long story short, I am now left with only 6 cherry saplings, 3 of which are doing fine, while 3 are struggling.

Only time will tell if any cherry saplings will survive my stupidity! *insert face palm*
