---
layout: post
category: gardening
---

Last year, we had a case where a mouse had snuck into out balcony and started eating the chestnuts we were trying to germinate. So, Jan 11th, following the last update I also had the idea to try and move the hydroponic oaks inside.

Last year the mouse had seemed only interested in the chestnuts and completely ignored the acorns (maybe they dont eat them? maybe when the chestnuts were there, the acorns were not appealing? I don't know, I'm not a mouseologist). However, I still didn't want to risk it.

As you can imagine, given the warmth inside our appartment, after just a few days of moving the acorns inside, they started to "wake up" with their roots shooting down and they started cracking their outer shells

![Hydroponic Acorns Awaken](/assets/images/gardening/hydroponic_oaks_awaken/hydroponic_oaks_awaken_0.jpg)

![Hydroponic Acorns Awaken](/assets/images/gardening/hydroponic_oaks_awaken/hydroponic_oaks_awaken_1.jpg)

![Hydroponic Acorns Awaken](/assets/images/gardening/hydroponic_oaks_awaken/hydroponic_oaks_awaken_2.jpg)

![Hydroponic Acorns Awaken](/assets/images/gardening/hydroponic_oaks_awaken/hydroponic_oaks_awaken_3.jpg)

On a side note, one of the nicest things about growing plants in water bottles and vessels, is that I find that their roots are almost a work of art on their own, that is gorgeous to look at and think of all the systems that work to make them end this way and to think of coding procedural generation systems to give 3D looking roots.

This last topic is a favourite of mine, and one I've tried exploring before using [L-systems](https://en.wikipedia.org/wiki/L-system), but that is a topic for another post. For now, here are some pretty oak roots.

![Root Detail](/assets/images/gardening/hydroponic_oaks_awaken/root_detail_0.jpg)

![Root Detail](/assets/images/gardening/hydroponic_oaks_awaken/root_detail_1.jpg)

Fast forward a few days later (I know, lots going on, but it has been a month since my last update, and I have been lazy) and now those cracking acorns have started shooting up sprouts!

![Sprouting Acorns](/assets/images/gardening/hydroponic_oaks_awaken/sprout_0.jpg)

![Sprouting Acorns](/assets/images/gardening/hydroponic_oaks_awaken/sprout_1.jpg)

So, I moved them under some IKEA growth lights we already bought a few years ago, and lo and behold, we now have tiny oak-lings :)

![Under Growth Lights](/assets/images/gardening/hydroponic_oaks_awaken/under_growth_lights_0.jpg)

![Under Growth Lights](/assets/images/gardening/hydroponic_oaks_awaken/under_growth_lights_1.jpg)

![Under Growth Lights](/assets/images/gardening/hydroponic_oaks_awaken/under_growth_lights_2.jpg)

![Under Growth Lights](/assets/images/gardening/hydroponic_oaks_awaken/under_growth_lights_3.jpg)

I still have no idea what the long-term plan for hydroponic oaks are; it should be obvious that are going to *very quickly* outgrow the 60cm window space we have for them, but I am hoping that by then, it will be mild enough to start moving them to the balcony, and then eventually outside.

For now though, the tiny yet most definitely white-oak patterend leaves bring me a lot of joy in their mini size. It is like looking at a baby animal that has all the features of the full grown adult, but in an impossibly tiny package!

I also have some pretty exciting news regarding some Lychee and some Chestnuts, but I'll leave that as a teaser for a future post. Stay tuned ;)
