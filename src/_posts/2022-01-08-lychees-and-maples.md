---
layout: post
category: gardening
---

After the xmas and new year's break, we are back to checking some of the seed germination projects we had started at the end of 2021.

In Week 50 2021, my wife and kids had bought some store [Lychee fruit](https://en.wikipedia.org/wiki/Lychee). Seeing as the fruits come with these awesome fresh and healthy looking seeds, my mind immediately went to the same thing I always think of; I wondered if I could germinate and grow them into small lychee plants :)

So the first thing, after a dozen youtube videos on how to do so, was to prepare them. We picked the seeds that looked round, full and with an uncracked skin and then wrapped them in a wet paper towel and then into a bunch of aluminium foil and into a dark drawer for two weeks.

![The packed foil with the seeds inside](/assets/images/gardening/lychees_and_maples/00_foil_packed.jpg)

Two weeks later, and that time is now :). My daughter & I unpacked the seeds and to our surprise, a little over half of the seeds had actually sprouted roots!

![Unpacked foil showing root sprouting seeds](/assets/images/gardening/lychees_and_maples/01_unpacked_foil.jpg)

And here is a closeup shot showing the roots sprouting from some of those seeds

![Closeup of roots from sprouting seeds](/assets/images/gardening/lychees_and_maples/02_germinated_seeds_closeup.jpg)

---

On another note, another thing we had collected all the way in October, 2021, were local [Maple Seeds](https://en.wikipedia.org/wiki/Maple). Now, the maple seeds themselves, (aside from the wings) come in a hard shell that needs to be cracked. This year, unlike the cherry seeds, I decided to try and crack them naturally.

Essentially, we placed the seeds in a plastic container full of wet soil in our balcony. As the temperatures dropped over the few weeks, the water in the mud would freeze, expand and as such, crush the outer shell of the seeds without damaging the inner embryo. This is a process called [Stratification](https://en.wikipedia.org/wiki/Stratification_(seeds)) and is a very common requirement in cold climate species. It is nature's way of only cracking the seeds in the coldest times of the year, and hence getting the new saplings ready in time for spring.

This is how those seeds looked, taking them out of the earth yesterday. Notice how ragged the outer shell looks, because of a combination of the frost repeatedly "micro-crushing" the outer shell.

![Ragged outershells of maple seeds after being battered by ground frost](/assets/images/gardening/lychees_and_maples/04_maples_out_of_earth.jpg)

After taking them out of the soil in this state and gently washing them with water, it was very easy to peel off the outer shell (that was no longer hard), leaving the inner embryo ready to be planted. Technically in nature, this separation doesn't need to happen, but since we are dealing with significantly smaller numbers than a single tree drops, I wanted to manually do this to ensure higher rate of germination

![The inner "embryo" seeds of the maple](/assets/images/gardening/lychees_and_maples/06_maple_inner_seeds.jpg)

---

The next step was straight forward, take all those maple and lychee seeds, and plant them in two empty egg cartons full of soil, and place them under our growth lights

![Planted under the growth lights](/assets/images/gardening/lychees_and_maples/07_planted_in_egg_cartons.jpg)

This was two days ago, and as of today, you can see that a couple of the lychees have already started sprouting stems.

![Sprouting Lychee stems](/assets/images/gardening/lychees_and_maples/08_sprouting_lychee.jpg)

![Sprouting Lychee stems](/assets/images/gardening/lychees_and_maples/09_sprouting_lychee_1.jpg)

Those two (and any others that sprout stems) will then be transported to slightly deeper pots, to allow for proper root development. But that, is for another post ;)
