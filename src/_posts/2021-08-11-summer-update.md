---
layout: post
category: gardening
---

After a long hiatus, I am back with updates. 

We have gone on a summer vacation (yay vaccination!) and have come back after three weeks of not looking at the lot much. 

However, as you can see from the image below, things have gone _very_ well. 

![theilot has definitely grown!](/assets/images/gardening/summer_update_20210730.jpg)

![close up b&w focus on the apple tree to make it more visible](/assets/images/gardening/summer_update_20210730_apple.jpg)

The marigolds have become monstrous! There are also wild poppies, and the apple tree is a whopping 110cm tall now! 

Surprisingly, the strawberries and Pansies seem to be enjoying the shade provided by the marigolds. 

I think next year I shall try and add a little more organisation to the lot, but for now, I am enjoying how natural it looks. 

Lastly, we have noticed that on a daily basis there are at least 3-4 bees around the lot. Talk about a win! 
