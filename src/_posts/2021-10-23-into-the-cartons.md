---
layout: post
category: gardening
---

The seeds have had enough time to sit out in the cold, time to check on them

First off, all acorns are removed from the soil. The ones that have not even started to grow a tap root are discarded (the maple & chestnuts are not ready yet, they need more time)

![The acorns removed from the soil](/assets/images/gardening/1_soil.jpg)

Since I do not have infinite space (nor funds) I have been collecting milk cartons into which to plant the rooting acorns.

I think milk cartons work really well because they are flexible enough to expand and contact with the soil if the soil moisture freezes. But more importantly, they work well by providing a deep enough "pot" for the tap root to grow down unrestricted. All nuts have taproos that grow very deep down and growing nut trees in small pots means the tap root will grow circular, which would make it quite weak when replanting them outside in soil later on.

Of course, even with milk cartons, I still do not have infinite space. In total, I have about 32 milk cartons and about 8 more deep enough pots. So the next step was sorting the acorns into piles based on how healthy and long the tap root looked, so my daughters & I can (in most unscientific fashion) pick the healthiest 40.

![sorting the acorns into piles](/assets/images/gardening/2_piles.jpg)

So, that was really it. With the acorns sorted and the milk cartons filled with peat moss enfused potting soil, all that was left to it was to plant the acorns.

They will sit outside in the balcony all winter, because one mistake we did last year was to try and give them artificial light, which while good at seedling level, eventually meant that during the harshest & darkest times of winter, they were woken up. This year, I intend to give them no artificial assistance so that their "bio clock" is in sync with the weather

![The acorns planted into their winter home](/assets/images/gardening/3_cartons.jpg)
