---
layout: post
category: gardening
---

A few people have asked for an update on the oak saplings we have planted out in the park next to our place.

It snowed a couple of times over the last couple of weeks, so I thought today would be the perfect day to get some pictures of the oaks in winter!

As you can see below, all the saplings have lost their leaves (of course) but the buds are still nice and fresh looking.

I find they look awesome in winter, they look like they are _just about_ to bloom any minute now.

Of course, they will stay in this _dormant_ state for the next two to three months at least.

![winter oak sapling 0](/assets/images/gardening/winter_oak_0.jpg)

![winter oak sapling 1](/assets/images/gardening/winter_oak_1.jpg)

![winter oak sapling 2](/assets/images/gardening/winter_oak_2.jpg)

![winter oak sapling 3](/assets/images/gardening/winter_oak_3.jpg)

![winter oak sapling 4](/assets/images/gardening/winter_oak_4.jpg)

It is hard to get the phone camera to focus on the very thin saplings, but hopefully this gives you an idea of how they look over winter.
