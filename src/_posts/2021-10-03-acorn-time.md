---
layout: post
category: gardening
---

My daughters & I have gone out and collected a little over a hundred acorns from local white oak trees

In addition, we also collected chestnuts & maple seeds

All seeds were left to soak in water, and any that floated were discarded; that was usually a sign that they had air in them, which indicated worms and other insects had already gotten to them

![Soaking the seeds, the ones that float are discarded](/assets/images/gardening/00_soaking.jpg)

All good seeds were then put into plastic containers, covered in soil, made moist and then left out in the balcony. Our balcony is not insulated nor heated, so cold stratification should still be free too take its course

![packing the collected seeds](/assets/images/gardening/0_start.jpg)
