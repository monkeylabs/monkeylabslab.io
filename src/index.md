---
layout: default
title: tekmonkey.xyz
---

---
### I'm Ali 'tekmonkey' Helmy
Husband & Father, Journeyman Engineer, Guerilla Gardener, Dreamy Robotocist, Slow Cyclist, Thalassophile, SciFi Nerd & Futurist

This is my home on the web

---
### Projects
For no reason other than to entertain myself and to learn a list of all the projects that I would like one day to work on -> [Read More](/projects.html)

---
### Gardening
A log to document my efforts to:
* Try and make local tree propagation a cheap sustainable process, rather than a lucky fluke.
* Implement some permaculture techniques with said gardening lot.

[Read More](/gardening.html)

---
### Art
Occaisionally I like to try my hand at simple pixel or voxel art -> [Check them out](/art.html)

---
### Meta
I hang out on twitter [@_tekmonkey](https://twitter.com/_tekmonkey), post pictures on [instagram](https://www.instagram.com/tek_monkey/) & host my code on [gitlab](https://gitlab.com/tekmonkey)<br>

My website and a lot of my projects were inspired by [Manfred Mornhinweg’s.](https://ludens.cl/index.html)

That means, it is intentionally:
* Plain. No distractions.
* About science, technology, engineering or mathematics.
* A good citizen of the internet. (Small page sizes. no JS. HTTPS.)
