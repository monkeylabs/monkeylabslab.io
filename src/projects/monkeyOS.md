---
layout: default
title: Monkey OS!
---

---
### Why?
For no reason other than to entertain myself and to learn

Because some times you get bored of writing high level game engine code and want to just deal with the machine hardware and nothing else

---
### End Goal?
Nothing really. I don't ever imagine this being used except for my own personal entertainment and education.

One day I may push it all the way to be able to boot into it from a USB disk, but thats about it

UPDATE: Rethinking about it again and about [this awesome SMBC comic](https://www.smbc-comics.com/index.php?id=2158) the end goal is to, obviously, be able to run DOOM

![Motivation](/assets/images/projects/monkeyOS/motivation.jpg)

---
### Current Status?

* ~~Implement bootloader~~
* ~~Implement FAT12 driver in asm / C~~
* ~~Implement stage 2 bootloader that can boot into a C environment~~
* ~~Implement putc & puts calls through x86 interrupts and \_cdecl~~
* ~~Print hello world from C from puts calls~~
* Implement printf
* Implement rest of stdlib
* Port DOOM to monkeyOS
* ... ?
* Profit!

---
### Where can I see more?
You can checkout [the project's git repo](https://gitlab.com/monkeylabs/monkeyOS)

---
### Screenshots
Do try & contain your amazement

![Latest](/assets/images/projects/monkeyOS/latest_screenshot.jpg)
