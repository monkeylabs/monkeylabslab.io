export Path="$HOME/.local/share/gem/ruby/3.0.0/bin:$PATH"
export GEM_HOME="$HOME/gems"

alias jekyll="~/.local/share/gem/ruby/3.0.0/bin/jekyll"
alias jekyll_serve="~/.local/share/gem/ruby/3.0.0/bin/jekyll serve"
alias jekyll_build=" ~/.local/share/gem/ruby/3.0.0/bin/jekyll build --destination ../public"
